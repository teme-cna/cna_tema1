package services;

import io.grpc.Status;
import io.grpc.stub.StreamObserver;
import proto.PersonOuterClass;
import proto.PersonOuterClass.*;
import proto.PersonServiceGrpc;
import java.util.ArrayList;
import java.util.List;

public class Person extends PersonServiceGrpc.PersonServiceImplBase {

    List<PersonOuterClass.Person> people = new ArrayList<>();

    @Override
    public void getPerson(PersonRequest request, StreamObserver<PersonResponse> responseObserver) {
        System.out.println("getPerson accessed");

        PersonResponse.Builder response = PersonResponse.newBuilder();
        PersonOuterClass.Person.Builder person = null;

        PersonOuterClass.Person search = people.stream().filter(element -> element.getName().equals(request.getName())).findFirst().orElse(null);

        if (search == null) {
            Status status = Status.NOT_FOUND.withDescription("This person doesn't exist.");
            responseObserver.onError(status.asRuntimeException());
        } else
            person = search.toBuilder();

        response.setPerson(person);
        responseObserver.onNext(response.build());
        responseObserver.onCompleted();
    }

    @Override
    public void setPerson(PersonOuterClass.Person request, StreamObserver<Empty> responseObserver) {
        System.out.println("setPerson accessed");
        people.add(request);

        System.out.println(request);
        System.out.println("All the people:\n" + this.people);

        Empty.Builder response = Empty.newBuilder();
        responseObserver.onNext(response.build());
        responseObserver.onCompleted();
    }

    @Override
    public void getPeople(Empty request, StreamObserver<PersonOuterClass.Person> responseObserver) {
        if (this.people.size() != 0) {
            for (PersonOuterClass.Person person : this.people) {
                responseObserver.onNext(person);
            }
            responseObserver.onCompleted();
        } else {
            Status status = Status.NOT_FOUND.withDescription("There is no people added.");
            responseObserver.onError(status.asRuntimeException());
        }
    }
}
