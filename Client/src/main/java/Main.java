import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.StreamObserver;
import proto.PersonOuterClass;
import proto.PersonServiceGrpc;
import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 8999).usePlaintext().build();
        PersonServiceGrpc.PersonServiceStub personStub = PersonServiceGrpc.newStub(channel);

        System.out.println("Options:");
        System.out.println("1. Add a person.");
        System.out.println("2. Show an added person.");
        System.out.println("3. Show all added people.");

        System.out.println("\n0. QUIT\n");

        boolean isConnected = true;
        while (isConnected) {
            Scanner input = new Scanner(System.in);
            System.out.print("Choose your option: ");
            int option = input.nextInt();

            switch (option) {
                case 1: {
                    Scanner read = new Scanner(System.in).useDelimiter("\n");

                    System.out.println("Person's CNP: ");
                    String cnp = read.next();

                    System.out.println("Enter the person's full name: ");
                    String name = read.next();

                    System.out.println("Person's age: ");
                    int age = read.nextInt();

                    System.out.println("Person's gender: ");
                    Arrays.stream(PersonOuterClass.Person.Gen.values()).forEach(System.out::println);
                    String sex = read.next();

                    personStub.setPerson(
                            PersonOuterClass.Person.newBuilder()
                                    .setCnp(cnp)
                                    .setName(name)
                                    .setAge(age)
                                    .setGen(PersonOuterClass.Person.Gen.valueOf(sex)).build(),
                            new StreamObserver<PersonOuterClass.Empty>() {
                                @Override
                                public void onNext(PersonOuterClass.Empty empty) {
                                }

                                @Override
                                public void onError(Throwable throwable) {
                                    System.out.println("Error : " + throwable.getMessage());
                                }

                                @Override
                                public void onCompleted() {

                                }
                            }

                    );
                    break;
                }
                case 2: {
                    Scanner read = new Scanner(System.in).useDelimiter("\n");
                    System.out.println("Enter the full name of the person: ");
                    String personName = read.next();

                    personStub.getPerson(PersonOuterClass.PersonRequest.newBuilder().setName(personName).build(), new StreamObserver<PersonOuterClass.PersonResponse>() {
                        @Override
                        public void onNext(PersonOuterClass.PersonResponse personResponse) {
                            System.out.println(personResponse);
                        }

                        @Override
                        public void onError(Throwable throwable) {
                            System.out.println("Error : " + throwable.getMessage());
                        }

                        @Override
                        public void onCompleted() {

                        }
                    });

                    break;
                }
                case 3: {
                    personStub.getPeople(PersonOuterClass.Empty.newBuilder().build(), new StreamObserver<PersonOuterClass.Person>() {
                        @Override
                        public void onNext(PersonOuterClass.Person person) {
                            System.out.println("CNP:" + person.getCnp() + "\n"
                                    + "Name: " + person.getName() + "\n"
                                    + "Age: " + person.getAge() + "\n"
                                    + "Gender: " + person.getGen() + "\n");
                        }

                        @Override
                        public void onError(Throwable throwable) {
                            System.out.println("Error : " + throwable.getMessage());
                        }

                        @Override
                        public void onCompleted() {

                        }
                    });
                    break;
                }
                case 0: {
                    isConnected = false;
                    break;
                }
                default:
                    System.out.println("Please select a number from 0 to 3!");
            }
        }
        channel.shutdown();
    }
}
